#ifndef MAIN_H_
#define MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "node.h"
#include "extra.h"

void menu();

int insert_tree(node** tree, char* data);

void print_tree(node* tree);

int height_tree(node* tree);

int find_tree(node* tree, char* data);

// extras

int nodes(node* tree);

int auto_add(node** tree, int num, int len);

void pretty_print(node* tree);

#endif
