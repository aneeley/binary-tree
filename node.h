#ifndef NODE_H_
#define NODE_H_

typedef struct node {
    char* data;
    struct node* left;
    struct node* right;
} node;

#endif
