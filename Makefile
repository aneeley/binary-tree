all: tree

tree: main.o extra.o
	c99 main.o extra.o -o tree

main.o: node.h main.h main.c
	c99 -c node.h main.h main.c

extra.o: node.h extra.h extra.c
	c99 -c node.h extra.h extra.c

clean:
	rm tree *.o *.gch

