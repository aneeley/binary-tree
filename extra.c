#include "extra.h"

// returns # of nodes in tree

int nodes(node* tree) {
    if(!tree) return 0;
    return 1 + nodes(tree->left) + nodes(tree->right);
}

// adds `num` random nodes with words of length `len`

int auto_add(node** tree, int num, int len) {
    for(int x = 0; x < num; x++)
        insert_tree(tree, rand_word(len));
}    

// creates random word of length `len`

char* rand_word(int len) {
    char* str = malloc(len * sizeof(char));
    for(int i = 0; i < len; i++)
        str[i] = rand_letter();
    return str;
}

// returns random lowercase letter

char rand_letter() {
    return 97 + rand() % 26;
}

// prints tree visually

void pretty_print(node* tree) {
    pretty_print_rec(tree, "", 1);
}

// recursive function for pretty_print

void pretty_print_rec(node* tree, char* indent, int last) {
    if(!tree) return;
    char buf[100];
    strncpy(buf, indent, sizeof(buf));
    printf("%s", buf);

    // printing root

    if(strlen(indent) == 0) {
        printf("───%s\n", tree->data);
        strncat(buf, "   ", sizeof(buf));
    }

    // printing first child

    else if(!last) {
        printf("├──%s\n", tree->data);
        strncat(buf, "│  ", sizeof(buf));
    }

    // printing last child

    else if(last) {
        printf("└──%s\n", tree->data);
        strncat(buf, "   ", sizeof(buf));
    }

    if(tree->left)  pretty_print_rec(tree->left, buf, !tree->right);
    if(tree->right) pretty_print_rec(tree->right, buf, 1);
}

