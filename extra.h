#ifndef EXTRA_H_
#define EXTRA_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "node.h"
#include "main.h"

int nodes(node* tree);

int auto_add(node** tree, int num, int len);

char rand_letter();

char* rand_word(int len);

void pretty_print(node* tree);

void pretty_print_rec(node* tree, char* indent, int last);

#endif

