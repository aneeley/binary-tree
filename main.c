#include "main.h"

int main() {
    node* root = NULL;
    char input[80];

    menu();

    while(fgets(input, sizeof(input), stdin) != NULL) {
        printf("\n");

        // remove newline char

        input[strlen(input) - 1] = '\0';

        // quit

        if(strcmp(input, "q") == 0) {
            puts("goodbye");
            exit(0);
        }

        // print

        else if(strcmp(input, "p") == 0) {
            print_tree(root);
        }

        // height

        else if(strcmp(input, "h") == 0) {
            printf("height of tree: %d\n", height_tree(root));
        }

        // add

        else if(strcmp(input, "a") == 0) {
            puts("enter the string(s) you would like to add");
            puts("type DONE when you're done\n");
            while(fgets(input, sizeof(input), stdin) != NULL) {
                input[strlen(input) - 1] = '\0';
                if(strlen(input) == 0) continue;
                if(strcmp(input, "DONE") == 0) break;
                printf("\n");
                insert_tree(&root, input);
            }
        }

        // find

        else if(strcmp(input, "f") == 0) {
            puts("enter string to search for:\n");
            fgets(input, sizeof(input), stdin);
            input[strlen(input) - 1] = '\0';
            printf("\n");
            if(find_tree(root, input))
                puts("found");
            else
                puts("not found");
        }
        
        // extras
        
        // # nodes

        else if(strcmp(input, "n") == 0) {
            printf("number of nodes: %d\n", nodes(root));
        }

        // auto add

        else if(strcmp(input, "A") == 0) {
            int num_nodes = nodes(root);
            auto_add(&root, 10, 5);
            printf("automatically added %d nodes\n", nodes(root) - num_nodes);
        }

        // pretty print

        else if(strcmp(input, "P") == 0) {
            pretty_print(root);
        }

        // error: blank command

        else if(input[0] == '\0') {
            puts("enter a command");
        }

        // error: invalid command

        else {
            printf("\"%s\" is an invalid command\n", input);
        }

        menu();
    }
    return 0;
}

void menu() {
    printf("\n");
	puts("A) automatically add nodes to the tree");
	puts("a) add nodes to the tree");
    puts("h) print height of tree");
	puts("P) pretty print the tree");
	puts("p) print the tree");
	puts("f) find a node in the tree");
    puts("n) print # of nodes in tree");
	puts("q) quit");
    printf("\n");
}

// adds node to tree, return 1 if successful

int insert_tree(node** tree, char* data) {
    if(!*tree) {
        *tree = malloc(sizeof(node));
        (*tree)->data = strdup(data);
        (*tree)->left = (*tree)->right = NULL;
        return 1;
    } else {
        if(strcmp(data, (*tree)->data) < 0)
            insert_tree(&(*tree)->left, data);
        if(strcmp(data, (*tree)->data) > 0)
            insert_tree(&(*tree)->right, data);
        if(strcmp(data, (*tree)->data) == 0)
            return 0;
    }
}

// prints tree in alphabetical order

void print_tree(node* tree) {
    if(tree) {
        print_tree(tree->left);
        puts(tree->data);
        print_tree(tree->right);
    }
}

// returns height of tree

int height_tree(node* tree) {
    if(!tree) return 0;
    int l_height = height_tree(tree->left);
    int r_height = height_tree(tree->right);
    if(l_height > r_height)
        return l_height + 1;
    else
        return r_height + 1;
}

// determines if data is in tree

int find_tree(node* tree, char* data) {
    if(!tree) return 0;
    if(strcmp(data, tree->data) < 0)
        return find_tree(tree->left, data);
    if(strcmp(data, tree->data) > 0)
        return find_tree(tree->right, data);
    return 1;
}

